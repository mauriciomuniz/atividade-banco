public class Conta {

    private int numeroAgencia;
    private int numeroConta;
    private Double saldo;
    private Cliente cliente;

    public Conta(int numeroAgencia, int numeroConta, Double saldo, Cliente cliente) {
        this.numeroAgencia = numeroAgencia;
        this.numeroConta = numeroConta;
        this.saldo = saldo;
        this.cliente = cliente;
    }

    public Object saca(double valorSaque){
        if (valorSaque > saldo) {
            saldo-=valorSaque;
            return saldo;
        } else {
            throw new ArithmeticException("Saldo insuficiente!");
        }

    }

    public double deposita(double valorDeposito){
        saldo+=valorDeposito;
        return saldo;
    }

    public int getNumeroAgencia() {
        return numeroAgencia;
    }

    public void setNumeroAgencia(int numeroAgencia) {
        this.numeroAgencia = numeroAgencia;
    }

    public Integer getNumeroConta() {
        return numeroConta;
    }

    public void setNumeroConta(Integer numeroConta) {
        this.numeroConta = numeroConta;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}
