public class Cliente {

    private String nome;
    private Integer idade;
    private String cpf;
    private TipoCliente tipoCliente;

    public Cliente(String nome, Integer idade, String cpf, TipoCliente tipoCliente) throws Exception {
        if (idade<18) {
            throw new ArithmeticException("Cliente menor de idade");
        }
        this.cpf = cpf;
        this.nome = nome;
        this.idade = idade;
        this.tipoCliente = tipoCliente;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getIdade() {
        return idade;
    }

    public void setIdade(Integer idade) {
        this.idade = idade;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public TipoCliente getTipoCliente() {
        return tipoCliente;
    }

    public void setTipoCliente(TipoCliente tipoCliente) {
        this.tipoCliente = tipoCliente;
    }
}
