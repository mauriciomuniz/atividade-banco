public class Main {

    public static void main(String[] args) throws Exception {

        try {
            Cliente cliente = new Cliente("gabriel", 22, "239329", TipoCliente.PERSONNALITE);

            Conta conta = new Conta(12, 102, 10.0, cliente);

            System.out.println(conta.getNumeroConta());

        } catch (ArithmeticException e) {
            System.out.println(e);
        }

    }
}
